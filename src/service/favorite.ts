import { render } from '..';
import { fillArray, getAllFromLocalStorage } from './utils/utils';

export function renderFavorite(): void {
    const movies = getAllFromLocalStorage();
    const fArray = fillArray(movies, ['col-12', 'p-2']);
    render(fArray, '#favorite-movies');
}
