export function createHtmlElement(tag: string): HTMLElement {
    return document.createElement(tag);
}
