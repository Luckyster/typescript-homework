import { initButtons } from './routes/buttons';
import { initLoadMore } from './routes/loadmore';
import { initSearch } from './routes/search';
import { renderFavorite } from './service/favorite';
import { renderRandomMovie } from './service/movie';

export async function render(
    moviesArray: Array<HTMLElement>,
    selector: string,
    paged = false
): Promise<void> {
    // TODO render your app here
    const filmContainer = document.querySelector(selector);
    if (filmContainer && !paged) {
        filmContainer.innerHTML = '';
    }
    moviesArray?.forEach((el) => filmContainer?.append(el));
}
(async () => {
    await renderRandomMovie();
    renderFavorite();
    initButtons();
    initSearch();
    await initLoadMore();
})();
