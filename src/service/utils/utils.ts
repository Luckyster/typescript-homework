import { Movie } from '../../interfaces/movie.interface';
import createMovie from '../movie';

export function fillArray(
    arr: Array<Movie>,
    classes: Array<string> = []
): Array<HTMLElement> {
    return arr.map((e) => createMovie(e, classes));
}

export function addListener(
    selector: string | HTMLElement,
    type: string,
    callback: () => void
): void {
    const element =
        typeof selector === 'string'
            ? document.querySelector(selector)
            : selector;

    element?.addEventListener(type, callback);
}

export function addToLocalStorage(id: number, movie: Movie): void {
    localStorage.setItem(id.toString(), JSON.stringify(movie));
}

export function removeFromLocalStorage(id: number): void {
    localStorage.removeItem(id.toString());
}

export function getFromLocalStorage(id: number): Movie | null {
    const item = localStorage?.getItem(id.toString());
    return item ? JSON.parse(item) : null;
}

export function getAllFromLocalStorage(): Array<Movie> {
    const allMovies = [];
    for (const [, val] of Object.entries(localStorage)) {
        allMovies.push(JSON.parse(val));
    }
    return allMovies;
}

export function getImage(url: string, fullWidth = false): string {
    const width = fullWidth ? 'original' : 'w500';
    return url?.length
        ? 'https://image.tmdb.org/t/p/' + width + url
        : 'http://' +
              window.location.host +
              '/public/assets/images/no-image.jpg';
}

export function getRandom(min: number, max: number): number {
    return Math.random() * (max - min) + min;
}
