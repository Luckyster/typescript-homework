import search from '../service/search';
import { addListener } from '../service/utils/utils';

export function initButtons(): void {
    document.querySelectorAll('#button-wrapper > input').forEach((el) => {
        if (el.hasAttribute('checked')) {
            search('movie/' + el?.getAttribute('id'));
        }
        addListener(el as HTMLElement, 'click', function (this: HTMLElement) {
            if (!this.hasAttribute('checked')) {
                document
                    .querySelectorAll('#button-wrapper > input')
                    .forEach((e) => e.removeAttribute('checked'));
                this?.setAttribute('checked', '');
            }
        });

        addListener(el as HTMLElement, 'change', function (this: HTMLElement) {
            if (this.hasAttribute('checked')) {
                const buttonID = this?.getAttribute('id');
                search('movie/' + buttonID);
            }
        });
    });
}
