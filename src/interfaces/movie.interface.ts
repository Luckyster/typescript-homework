export interface Movie {
    poster_path: string;
    backdrop_path: string;
    overview: string;
    original_title: string;
    release_date: string;
    id: number;
}
