import { render } from '..';
import server from '../server';
// import createMovie from '../service/movie';
// import search from '../service/search';
import { addListener, fillArray } from '../service/utils/utils';

export async function initLoadMore(): Promise<void> {
    addListener('#load-more', 'click', async function (this: HTMLElement) {
        const url = this.getAttribute('data-url') ?? '';
        const query = this.getAttribute('data-query') ?? '';
        const page = this.getAttribute('data-page') ?? '1';
        const moreMovies = await server(url, query, parseInt(page) + 1);
        const htmlMovies = fillArray(moreMovies, [
            'col-lg-3',
            'col-md-4',
            'col-12',
            'p-2',
        ]);
        await render(htmlMovies, '#film-container', true);
    });
}
