import search from '../service/search';
import { addListener } from '../service/utils/utils';

export function initSearch(): void {
    const input = document.querySelector('#search') as HTMLInputElement;
    addListener('#submit', 'click', function () {
        if (!input) {
            throw new Error('Empty Search');
        }
        search('search/movie', input?.value);
    });

    const searchButton = document.querySelector('#submit') as HTMLElement;
    input.addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            searchButton?.click();
        }
    });
}
