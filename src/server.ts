// import { SearchInterface } from './interfaces/search.response.interfase';
import { Movie } from './interfaces/movie.interface';

export default async function server(
    url: string,
    query = '',
    page = 1,
    id = ''
): Promise<Array<Movie>> {
    const apiKey = 'cad7c812acd7e6725d94e0ef8d731d56';
    const defaultPath = 'https://api.themoviedb.org/3/';
    const apiQuery = query ? `&query=${query}` : '';
    const apiPage = page ? `&page=${page}` : '';
    // const _url = `${defaultPath}${url}?api_key=${apiKey}&language=en-US${apiQuery}&page=1&include_adult=true`;
    const _url = `${defaultPath}${url}${id}?api_key=${apiKey}&language=en-US${apiQuery}${apiPage}`;

    const response = await fetch(_url);
    const responseJson = await response.json();
    if (!responseJson.results) {
        responseJson.results = [responseJson];
    }
    console.log(responseJson.results);
    if (id.length === 0) {
        const loadMore = document.querySelector('#load-more');
        loadMore?.setAttribute('data-url', url);
        loadMore?.setAttribute('data-page', page.toString());

        if (apiQuery !== '') {
            loadMore?.setAttribute('data-query', query);
        }
    }
    return responseJson.results;
}
