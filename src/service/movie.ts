import { Movie } from '../interfaces/movie.interface';
import { createHtmlElement } from './utils/html.utils';
import { getFromLocalStorage, getImage, getRandom } from './utils/utils';
import { like } from './like';
import server from '../server';
import { render } from '..';

export default function createMovie(
    movie: Movie,
    classes: Array<string> = [],
    fullTemplate = false
): HTMLElement {
    const {
        poster_path: image,
        backdrop_path: bgImage,
        release_date: date,
        original_title: title,
        overview: description,
        id,
    } = movie;
    const html = createHtmlElement('div');
    html?.classList.add(...classes);
    let innerHTML = '';
    if (!fullTemplate) {
        const isLiked = getFromLocalStorage(id) ? 'red' : '#ff000078';
        innerHTML = `<div class="card movie-js shadow-sm" data-id="${id}">
    <img src="${getImage(image)}">
    <svg xmlns="http://www.w3.org/2000/svg" stroke="red" fill="${isLiked}" width="50" height="50" class="bi bi-heart-fill position-absolute p-2 like like-js" viewBox="0 -2 18 22">
        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"></path>
    </svg>
    <div class="movie-body">
        <p class="movie-text truncate">${description}</p>
        <div class="
                d-flex
                justify-content-between
                align-items-center
            ">
            <small class="text-muted">${date}</small>
        </div>
    </div>
</div>`.replace('\n', '');
    } else {
        innerHTML = `<div class="col-lg-6 col-md-8 mx-auto" style="background-color: #2525254f">
        <h1 id="random-movie-name" class="fw-light text-light">
            ${title}
        </h1>
        <p id="random-movie-description" class="lead text-white">
            ${description}
        </p>
        </div>`;
        const randomMovieSection = document.querySelector(
            '#random-movie'
        ) as HTMLElement;
        randomMovieSection?.setAttribute(
            'style',
            `background-image:url(${getImage(bgImage, true)})`
        );
    }
    html.innerHTML = innerHTML;
    html.querySelector('.like-js')?.addEventListener(
        'click',
        function (this: HTMLElement) {
            like(movie);
            const moveSelector =
                '#film-container .movie-js[data-id="' +
                movie.id +
                '"] .like-js';
            const element = document.querySelector(moveSelector);
            if (element?.getAttribute('fill') === 'red') {
                element?.setAttribute('fill', '#ff000078');
            } else {
                element?.setAttribute('fill', 'red');
            }
        }
    );

    return html;
}

export async function getRandomMovie(): Promise<Movie> {
    const movies = await server('movie/upcoming');

    const sortedMovies = movies.sort((a, b) => a.id - b.id);
    const randomID = getRandom(1, sortedMovies[1]?.id).toFixed(0);
    const randomMovie = await server(`movie/`, '', 0, `${randomID}`);

    return !randomMovie || !randomMovie?.[0]?.id
        ? await getRandomMovie()
        : randomMovie?.[0];
}

export async function renderRandomMovie() {
    const randomMovie = await getRandomMovie();
    const movieHTML = createMovie(randomMovie, ['row', 'py-lg-5'], true);
    render([movieHTML], '#random-movie');
}
