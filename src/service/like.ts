import { Movie } from '../interfaces/movie.interface';
import { renderFavorite } from './favorite';
import {
    addToLocalStorage,
    removeFromLocalStorage,
    getFromLocalStorage,
} from './utils/utils';

export function like(movie: Movie): void {
    const itemID = movie.id ?? '';
    const isExist = getFromLocalStorage(itemID);
    if (isExist) {
        removeFromLocalStorage(itemID);
    } else {
        addToLocalStorage(itemID, movie);
    }
    renderFavorite();
}
