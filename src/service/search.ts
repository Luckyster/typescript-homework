import { fillArray } from './utils/utils';
import { render } from '../index';
import { Movie } from '../interfaces/movie.interface';
// import { SearchInterface } from './interfaces/search.response.interfase';
import server from '../server';

export default async function search(url: string, query = ''): Promise<void> {
    const searchResponse = await server(url, query);
    const results = searchResponse as Array<Movie>;

    const fArray = fillArray(results, ['col-lg-3', 'col-md-4', 'col-12', 'p-2']);

    render(fArray, '#film-container');
}

// export function addListener(
//     selector: string | HTMLElement,
//     type: string,
//     callback: () => void
// ): void {
//     const element =
//         typeof selector === 'string'
//             ? document.querySelector(selector)
//             : selector;

//     element?.addEventListener(type, callback);
// }
